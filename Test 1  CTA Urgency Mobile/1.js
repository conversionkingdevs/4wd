


function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function start(){
    var items = document.getElementsByClassName(".item");
    var newDiv = document.createElement("div"); 
    var newContent = document.createTextNode("Hi there and greetings!");
    
    jQuery(".item").each(function(){
        jQuery(this).find(".product-image").after('<div class="opt-container"></div>');
    });
}

defer(function(){

    jQuery("body").addClass("opt-1");
    start();
}, ".category-products");
