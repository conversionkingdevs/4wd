function defer(method, selector) {
    console.log('running');
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();  
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function() {
    if (jQuery(window).width() < 992) {
        jQuery('body').addClass('opt4-v1');

        jQuery('.product-shop .product-name').prependTo('.product-essential')
                                            .addClass('opt-title');
        
        jQuery('.average-review').insertAfter('.opt-title') .addClass('opt-review');

        jQuery('.product-img-box').addClass('opt-sliders');
        jQuery('.product-shop .dont-pay-more').addClass('opt-dontPayMore');
        jQuery('.product-shop .price-box').addClass('opt-priceBox');
        jQuery('.opt-priceBox .only-text').addClass('opt-onlyText');
        jQuery('.opt-dontPayMore .comparesaving').insertAfter('.opt-priceBox .price').addClass('opt-compareSaving');

        jQuery('.opt-priceBox .delv').insertAfter('.opt-priceBox').addClass('opt-maxFreight');

        jQuery('.opt-priceBox .price').addClass('opt-price');

        jQuery('<div class="opt-addTo"><div></div><div></div></div>').insertBefore('.product-shop .pricing-bottom');
        jQuery('.savetextbtn button').appendTo('.opt-addTo div:eq(1)').addClass('opt-buttonAddToCart');

        jQuery('.add-to-cart .productzip').appendTo('.opt-addTo div:eq(1)').addClass('opt-productZip');

        jQuery('.add-to-cart .qty-block').appendTo('.opt-addTo div:eq(0)').addClass('opt-qtyBlock');

        jQuery ('.add-to-cart').addClass('opt-addToCart');
        jQuery ('.zip').addClass('opt-zip');

        if (jQuery('.product-options').length > 0) {
            jQuery('.product-options').insertBefore('.opt-addTo').addClass('opt-productOptions');

            jQuery('.price-as-configured').addClass('invisible');

            jQuery('.product-options-bottom').addClass('opt-productOptionsBottom');
        }
        
        jQuery('.savetextbtn, .product-shop .row-product').addClass('invisible');
    }

}, '.product-view');

console.log('running test');