function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}


defer (function() {
    jQuery('body').addClass('opt76');
    for(var i=0;i<jQuery('.opt76 .actions').length;i++) {
        jQuery('.opt76 .actions:eq('+i+') .button.btn-details[title="Details"]').after(jQuery('.opt76 .actions:eq('+i+') .button.btn-cart'));
        jQuery('.opt76 .actions:eq('+i+') .button.btn-details[title="Details"]').after(jQuery('.opt76 .actions:eq('+i+') .availability.out-of-stock'));
    }

    if(jQuery(window).width() < 768) {
        for(var i=0;i<jQuery('.opt76 .actions').length;i++) {
            jQuery('.opt76 .actions:eq('+i+')').prepend(jQuery('.opt76 .actions:eq('+i+') button.button.btn-details:eq(1)'));
        }
    }


}, ".actions");