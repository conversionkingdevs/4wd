function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}


defer (function() {
    jQuery('body').addClass('opt76');
    for(var i=0;i<jQuery('.opt76 .actions').length;i++) {
        jQuery('.opt76 .actions:eq('+i+') .button.btn-cart').addClass('optWide');
        jQuery('.opt76 .actions:eq('+i+') .availability.out-of-stock').addClass('optWide')

        jQuery('.opt76 .product-shop:eq('+i+') .only-text').before(jQuery('.opt76 .product-shop:eq('+i+') .dont-pay-more .strike'));
        jQuery('.opt76 .product-shop:eq('+i+') .only-text').before(jQuery('.opt76 .product-shop:eq('+i+') h3'));
        jQuery('.opt76 .product-shop:eq('+i+') .only-text').before(jQuery('.opt76 .product-shop:eq('+i+') h3'));
        jQuery('.opt76 .product-shop:eq('+i+') .only-text').after(jQuery('.opt76 .product-shop:eq('+i+') .dont-pay-more'));
        jQuery('.opt76 .product-shop:eq('+i+') .row:eq(0)').addClass('optRow');
        jQuery('.opt76 .product-shop:eq('+i+') .row:eq(0) div:eq(0)').append(jQuery('.opt76 .product-shop:eq('+i+') .price-box'));
        jQuery('.opt76 .product-shop:eq('+i+') .product-name').after(jQuery('.opt76 .product-shop:eq('+i+') .dont-pay-more'));

        jQuery('.opt76 .product-shop:eq('+i+') .strike').text(jQuery('.opt76 .product-shop:eq('+i+') .strike').text().split('+')[0]);
        jQuery('.opt76 .actions:eq('+i+') button.button.btn-details:eq(2)').remove();
        jQuery('.opt76 .actions:eq('+i+') button.button.btn-details:eq(1)').remove();
        jQuery('.opt76 .actions:eq('+i+') button.button.btn-details:eq(0)').remove();
    }

}, ".actions");