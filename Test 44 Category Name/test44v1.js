function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
           
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function(){
        
    var menus = [
        {title: 'LED Driving Lights',          },
        {title: 'LED Light Bars',              },
        {title: 'Tools',                       },
        {title: 'Awning Mosquito Nets',        },
        {title: 'Awning Tents',                },
        {title: 'Battery and Battery Chargers',},
        {title: 'Camping',                     },
        {title: 'Camping Gear',                },
        {title: 'Awnings',                     },
        {title: 'Accessories',                 },
        {title: 'Cooking Gear',                },
        {title: 'Electrical Products',         },
        {title: 'Exhausts & Performance',      },
        {title: 'Air Compressors',             },
        {title: 'First Aid Kits',              },
        {title: 'Fuel Filter',                 },
        {title: 'Gazebo',                      },
        {title: 'Generators',                  },
        {title: 'Camping Fridges',             },
        {title: 'Clothing',                    },
        {title: 'Rear Drawers',                },
        {title: 'Recovery Accessories ',       },
        {title: 'Recovery Kit',                },
        {title: 'Roof Racks',                  },
        {title: 'Roof Top Tents',              },
        {title: 'Solar Panels',                },
        {title: 'Storage',                     },
        {title: 'LED Camping Lights',          },
        {title: 'Suspension',                  },
        {title: 'Swags',                       },
        {title: 'Winches',                     }
    ];

    menus.forEach(function(menu_order){
        jQuery(".main-container .nav-2 > ul > li > a span:contains("+menu_order.title+")").parent().parent().appendTo(jQuery(".main-container .nav-2 > ul"));
    });

    menus.forEach(function(menu_order){
        jQuery(".page > .nav-container .nav-2 > ul > li > a span:contains("+menu_order.title+")").parent().parent().appendTo(jQuery(".page > .nav-container .nav-2 > ul"));
    });

}, '.main-container .nav-2 > ul > li > a');


