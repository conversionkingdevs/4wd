function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}


defer (function() {
    jQuery('body').addClass('opt74');
    if(jQuery('.dont-pay-more .strike').length !== 0) {
        var optPrice = jQuery('.dont-pay-more .strike').text().split(" ")[1];
        jQuery('.price-box .only-text').text(optPrice);
        jQuery('.price-box .only-text').addClass('optSlash');
        jQuery('.add-to-cart .qty-block').after(jQuery('.dont-pay-more span.comparesaving'));
        jQuery('.product-view .product-shop .price-box').after(jQuery('span.comparesaving').clone());
    }
},'.comparesaving');