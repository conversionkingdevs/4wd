function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}


defer (function() {
    jQuery('body').addClass('opt74');
    jQuery('.price-box .only-text').remove();

},'.price-box .only-text');