function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

jQuery( document ).ready(function(){
    defer(function(){
        
        jQuery("#sticky-btn .tbl .tbl-row").prepend("<div class='tbl-cell'></div>");
        jQuery("#sticky-btn .tbl .tbl-row").append("<div class='tbl-cell'></div>");
        jQuery("#sticky-btn .tbl .tbl-row .tbl-cell:eq(3)").append("<div class='close-btn'><span class='texts'>Close this</span><span class='cross'></span></div>");
        
        setup();

        jQuery("#sticky-btn .close-btn").click(function() {
            jQuery("#sticky-btn").fadeOut(400, function() {
                jQuery("#sticky-btn").remove();
            });
        });
        

    }, "#sticky-btn .tbl-cell:eq(2)");
});

function setup() {
    jQuery(window).scroll(function () {
        var scroll = jQuery(window).scrollTop();
        if(jQuery(window).width() >= 768) {
            if(scroll > 800 && jQuery("#sticky-btn").css("display") === "none") {
                jQuery("#sticky-btn").fadeIn(400);
            } else if (scroll <= 800 && jQuery("#sticky-btn").css("display") === "block") {
                jQuery("#sticky-btn").fadeOut(400);
            }
        }
    });
}