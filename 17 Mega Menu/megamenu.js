function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}
function start () {
  jQuery('body').addClass('OptimizelyTest');


/*******************************************/
/***** Layout ******************************/
/*******************************************/

/***** General *****/
jQuery(".OptimizelyTest .swipe-menu ul.links").wrap('<div class="links-wrapper" id="menu-top"/>');
jQuery(".OptimizelyTest .swipe-menu ul.links").html(jQuery(".OptimizelyTest #concept-menu ul.nav.navbar-nav").html());
jQuery(".OptimizelyTest .swipe-menu ul.links li ul.dropdown-menu").removeClass('dropdown-menu');
jQuery(".OptimizelyTest .swipe-menu ul.links li.text-center > a").addClass('dropdown-toggle');
jQuery(".OptimizelyTest .swipe-menu .home-link").prependTo(jQuery(".OptimizelyTest .swipe-menu ul.links")).wrap('<li />');
jQuery(".OptimizelyTest .swipe-menu .home-link").prepend('<i class="fa fa-home" />'); 

jQuery(".OptimizelyTest .swipe-menu ul.links li").each(function(){
    $this = jQuery(this);
    if($this.find('> br').length > 0){
        $this.remove();  
    };
});

jQuery(".OptimizelyTest .swipe-menu ul.links #smithfield-tile").parents('li[class*="col-sm-"]').remove();


jQuery(".OptimizelyTest .swipe-menu ul.links .category-image").each(function(){
    $this = jQuery(this);
    $this.parents('li[class*="col-sm-"]').remove();
});


jQuery('.OptimizelyTest .swipe-menu ul.links ul.mega-dropdown-menu li.dropdown-header').each(function(){
    $this = jQuery(this);
    if($this.find('a').length == 0 && $this.html() != '' && $this.html() != '&nbsp;'){
        $this.wrapInner('<a href="#" class="custom-dropdown-toggle"></a>');  
    };
});


jQuery(".OptimizelyTest .swipe-menu ul.links li ul").each(function(){
    $this = jQuery(this);
    $this.find('li[class*="col-sm-"] ul').unwrap();
    $this.find('li').unwrap('ul');
    $this.find(".dropdown-header").each(function(){
        jQuery(this).nextUntil('.dropdown-header').appendTo(jQuery(this)).wrapAll('<ul />');
    });
});


jQuery('.OptimizelyTest .swipe-menu ul.links a').each(function(){
    $this = jQuery(this);
    if($this.html() == '' && $this.text() == ''){
        $this.parent().remove();
    };
});


jQuery('.OptimizelyTest .swipe-menu ul.links h3').each(function(){
    $this = jQuery(this);
    $this.contents().unwrap();
});

var initialHeight;
jQuery(".swipe-control").click(function(){
    initialHeight = jQuery('.OptimizelyTest .swipe-menu ul.links').height()
    setTimeout(function(){
        
        jQuery('.OptimizelyTest .swipe-menu ul.links').css('left','0')
        jQuery('.OptimizelyTest .swipe-menu .links-wrapper').css('height','auto');
        jQuery('.OptimizelyTest .swipe-menu ul.links li').removeClass('active');
        jQuery('.OptimizelyTest .swipe-menu ul.links a').removeClass('active');
    },500);
});




/***** Second Level Menu *****/
jQuery('.OptimizelyTest .swipe-menu ul.links > li.dropdown').each(function(){
    $this = jQuery(this);
    $html = $this.find('> a').html();
    $this.find('> ul').prepend('<li><a class="dropdown-back-button" href="#">'+$html+'</a></li>');
});


jQuery('.OptimizelyTest .swipe-menu ul.links > li > a.dropdown-toggle').click(function(e){
    e.preventDefault();
    window.location.href = "#menu-top";
    $this = jQuery(this);
    jQuery('.OptimizelyTest .swipe-menu ul.links > li').removeClass('active');
    $this.addClass('active');
    $this.parent().addClass('active');
    jQuery('.OptimizelyTest .swipe-menu ul.links').animate({left:-220},300);
    jQuery('.OptimizelyTest .swipe-menu .links-wrapper').animate({height:$this.parent().find('> ul').height()},300);
    return false;
});


jQuery('.OptimizelyTest .swipe-menu ul.links > li > ul > li > a.dropdown-back-button').click(function(e){
    e.preventDefault();
    jQuery('.OptimizelyTest .swipe-menu ul.links li').removeClass('active');
    jQuery('.OptimizelyTest .swipe-menu ul.links a').removeClass('active');
    jQuery('.OptimizelyTest .swipe-menu ul.links').animate({left:0},300);
    jQuery('.OptimizelyTest .swipe-menu .links-wrapper').animate({height:initialHeight},300);
    return false;
});



/***** Third Level Menu *****/
jQuery('.OptimizelyTest .swipe-menu ul.links > li > ul > li.dropdown-header').each(function(){
    $this = jQuery(this);
    $html = $this.find('> a').html();
    $this.find('> ul').prepend('<li><a class="dropdown-back-button" href="#">'+$html+'</a></li>');
});

jQuery('.OptimizelyTest .swipe-menu ul.links > li > ul > li > a.dropdown-toggle, .OptimizelyTest .swipe-menu ul.links > li > ul > li > a.custom-dropdown-toggle').click(function(e){
    e.preventDefault();
    window.location.href = "#menu-top";
    $this = jQuery(this);
    jQuery('.OptimizelyTest .swipe-menu ul.links > li > ul > li').removeClass('active');
    $this.addClass('active');
    $this.parent().addClass('active');
    jQuery('.OptimizelyTest .swipe-menu ul.links').animate({left:-440},300);
    jQuery('.OptimizelyTest .swipe-menu .links-wrapper').animate({height:$this.parent().find('> ul').height()},300);    
    return false;
});


jQuery('.OptimizelyTest .swipe-menu ul.links > li > ul > li > ul > li > a.dropdown-back-button').click(function(e){
    e.preventDefault();
    $this = jQuery(this);
    jQuery('.OptimizelyTest .swipe-menu ul.links li ul li').removeClass('active');
    jQuery('.OptimizelyTest .swipe-menu ul.links li ul li a').removeClass('active');
    jQuery('.OptimizelyTest .swipe-menu ul.links').animate({left:-220},300);
    jQuery('.OptimizelyTest .swipe-menu .links-wrapper').animate({height:$this.parents('li.active > ul').height()},300);
    return false;
});


}
defer(start, '#concept-menu');