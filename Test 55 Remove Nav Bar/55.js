var bannerData = [
    {
        "img": '<img src="//cdn.optimizely.com/img/8495021062/07e06961478345929de85f0c2df1ef79.png">',
        "text": '<span>1. Your Details</span>',
        "onclick": function(){
            moveTo(0);
        },
        "class": "details"
    },
    {
        "img": '<img src="//cdn.optimizely.com/img/8495021062/29fe74fe39c54050b536b996a5218799.png">',
        "text": '<span>2. Shipping</span>',
        "onclick": function(){
            moveTo(1);
        },
        "class": "shipping"
    },
    {
        "img": '<img src="//cdn.optimizely.com/img/8495021062/5a209c7362504a68b6d0371bb8faafef.png">',
        "text": '<span>3. Payment</span>',
        "onclick": function(){
            moveTo(2);
        },
        "class": "payment"
    }
];

function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector) }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector) }, 50);
    }    
}

function checkState(){
    waitForLoading(
        function(){
            var plsWait = jQuery('.please-wait');
            var pauseCheck = false;
            jQuery.each(plsWait, function (i, item) {
                if (item.style.display !== 'none' && !pauseCheck) {
                    pauseCheck = true;
                };
            });
            if (pauseCheck) {
                return true;
            } else {
                return false;
            };
        },
        function(){
            setTimeout(function () {
                setState();
            } ,150)
        }, 333);
};

function setState(){
    var value = 0;
    var id = jQuery("#checkoutSteps > .active").attr("id");
    var shipping = jQuery(".control input[type='radio']:eq(0)").prop("checked");
    switch(id){
        case "opc-billing":
            value = 0;
            break;
        case "opc-shipping":
        case "opc-shipping_method":
            value = 1;
            break;
        case "opc-review":
        case "opc-payment":
            value = 2;
            break;
        default:
            value = 3;
            console.log("invalid id");
            break;
    }
    jQuery("body").attr("opt-shipping-state", shipping);
    jQuery("body").attr("opt-checkout-state", value);
}

function finishedLoading(){

}

function waitForLoading(condition, callback, pollSpeed){
    if(condition()){
        setTimeout(function(){
            waitForLoading(condition, callback, pollSpeed);
        },pollSpeed);
    } else {
        callback();
    }
}

function addCallbacks(){
    jQuery(".control input[type='radio']").click(function(){
        checkState();
    });

    jQuery(".back-link a").click(function(){
        checkState();
    });

    jQuery("button").click(function () {
        checkState();
    });

    jQuery(".opt55-banner-icon").click(function(){
        
        bannerData[parseInt(jQuery(this).attr("data-value"))]["onclick"]();
    });
}

function moveTo(index){
    jQuery("#checkoutSteps > li:eq("+index+") .step-title").click();
}

function addHtml() {
    var underLine = jQuery('#legacy-menu').html('');
    var banner = jQuery('.header .row. .col-sm-10').removeClass('col-sm-10').addClass('opt55-banner col-sm-7').html('<div class="opt55-banner-container"></div>');

    for (var i = 0; i < bannerData.length; i ++) {
        jQuery('.opt55-banner-container').append('<div class="'+bannerData[i]["class"]+' opt55-banner-icon" data-value="'+i+'"><div class="opt55-banner-img">'+bannerData[i]["img"]+'</div>'+bannerData[i]["text"]+'</div>');
    }
};

function start(){
    jQuery("body").addClass("opt-55");
    addHtml();
    checkState();
    addCallbacks();
    setInterval(function () {
        setState();
    }, 1000);
}

defer(function(){
    start();
}, "#checkoutSteps");