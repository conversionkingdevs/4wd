function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

jQuery( document ).ready(function(){

    //Waits for review tab to exist, does not move it to first position then clicks on it
    defer(function(){
    var tab = jQuery(".product-collateral .tabs > #category_product_description_reviews_tab");

        tab.click();

    }, ".product-collateral .tabs > #category_product_description_reviews_tab");
});