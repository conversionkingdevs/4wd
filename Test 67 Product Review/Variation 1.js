function defer(method, selector) {
    if (window.jQuery) {
        if (jQuery(selector).length > 0){
            method();
        } else {
            setTimeout(function() { defer(method, selector); }, 50);
        }  
    } else {
         setTimeout(function() { defer(method, selector); }, 50);
    }    
}

defer(function () {
  jQuery('.nav-container').prepend('<a target="_blank" href="https://www.productreview.com.au/p/4wd-supacentre.html" class="optReview"> <img width="160px" src="//d2uod8gew2p4yv.cloudfront.net/badge/322467/light-hd.png" alt="4WD Supacentre reviews"> </a>');

},".nav-container");