function replaceImg(el, src, remove) {
    jQuery(el).attr('src', src);
    if (remove == true) {
        jQuery(el).parent('a').find('img:nth-child(n+2)').remove();
    }
    jQuery(el).parent('a').addClass('optKeep');
}

replaceImg('.lhsPromo a[href="https://www.4wdsupacentre.com.au/competitions"] img', '//placehold.it/270x100', true);
replaceImg('.lhsPromo a[href="https://www.4wdsupacentre.com.au/stock-clearance.html"]:eq(0) img', '//placehold.it/270x100', true);
replaceImg('.lhsPromo a[href="https://www.4wdsupacentre.com.au/products/generators-8/adventure-kings-3-5kva-3500w-generator.html"] img', '//placehold.it/270x100', true);
replaceImg('.lhsPromo a[href="https://www.4wdsupacentre.com.au/products/camping-gear.html"] img', '//placehold.it/270x100', true)

jQuery('.lhsPromo a:not(.optKeep)').remove();