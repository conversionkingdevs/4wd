jQuery('body').addClass('opt53')

var getcat = jQuery('.breadcrumbs').find('li:eq(2) a').text();

switch (getcat) {
        case "SWAGS":
        var text1 ="Quality Canvas",
            text2 ="Waterproof",
            text3 ="Easy Setup",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Swags/quality canvas.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Swags/Waterproof.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Swags/easy setup.png"
        break;
        
        case "Awnings":
        var text1 ="UPF 50+ and Waterproof",
            text2 ="Setup in Seconds",
            text3 ="Thousands Sold",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Awnings/upf 50 and waterproof.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Awnings/set up in seconds.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Awnings/thousands sold.png"
        break;
        
        case "Accessories":
        var text1 ="Insanely Popular with Aussie 4WDers and Campers",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Accessories/insanely popular with aussie 4wders and campers.png"
        break;
        
        case "Air Compressors":
        var text1 ="Torture Tested on 4WD Action DVD",
            text2 ="Built Tough for Harsh Aussie Conditions",
            text3 ="Lightning Fast Tyre Pressure Management"
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Air Compressors/torture tested on 4WD action DVD.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Air Compressors/built tough for harsh aussie conditions.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Air Compressors/Lightning fast tyre pressure management.png"
        break;
        
        case "Awning Tents":
        var text1 = "Quality RipStop Polyester",
            text2 = "Simple Setup",
            text3 = "Packs Away Small"
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Awning Tents/quality ripstop polyester.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Awning Tents/simple set up.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Awning Tents/packs away small.png"
        break;
        
        case "Battery Chargers":
        var text1 ="Price Match Guarantee ",
            text2 ="Safer, Smarter Charging"
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Battery Chargers/price match guarantee.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Battery Chargers/safe smarter charging.png"
        break;
        
        case "Camping Gear":
        var text1 ="Made Tough For Aussie Campers",
            text2 ="Premium Quality Materials",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Camping gear/made tough for aussie campers.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Camping gear/premium quality material.png"
        break;
        
        case "Electrical Products":
        var text1 ="Built for Aussie 4WDers",
            text2 ="Reliable Electronics",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Electrical Products/built for aussie 4wders.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Electrical Products/reliable electronics.png"
        break;
        
        case "Exhaust Systems":
        var text1 ="Aussie Made",
            text2 ="Mandrel Bent",
            text3 ="Tough Construction",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Exhaust Systems/Aussie made.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Exhaust Systems/Mandrel bent.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Exhaust Systems/tough construction.png"
        break;
        
        case "First Aid Kits":
        var text1 ="Made for the Aussie Bush",
            text2 ="Durable and Rugged Unit",
            text3 ="Top Quality Parts",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/First Aid Kits/made for aussie bush.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/First Aid Kits/durable and rugged unit.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/First Aid Kits/top quality parts med kit.png"
        break;
        
        case "Fridges / Coolers":
        var text1 ="Built Tough",
            text2 ="Top Quality Parts",
            text3 ="Efficient Construction",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Fridges%20_%20Coolers/built%20tough.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Fridges%20_%20Coolers/top quslity part.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Fridges%20_%20Coolers/efficient construction.png"
            
        break;
        
        case "Fuel Filter":
        var text1 ="Trusted By Aussie 4WDers",
            text2 ="Top of the Line System",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Fuel Filters/trusted by aussies 4wders.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Fuel Filters/top of the line system.png"
        break;
        
        case "Gazebo":
        var text1 ="Simple, Strong Design",
            text2 ="Quality Materials",
            text3 ="UPF 50+ and Waterproof",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Gazebo/simple strong design.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Gazebo/quality materials.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Gazebo/upf 50 and waterproof.png"
        break;
        
        case "Generators":
        var text1 ="Quite and Reliable",
            text2 ="Lightweight and Tough",
            text3 ="Super Easy to Start",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Generators/quiet and reliable.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Generators/tough and lightweight.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Generators/super easy to start.png"
        break;
        
        case "LED Driving Lights":
        var text1 ="Tough-as-guts Construction",
            text2 ="Trusted by 1000s of Aussie",
            text3 ="Insanely Bright LEDS",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/LED Driving Lights/tough as gets construction.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/LED Driving Lights/trusted by 1000's of aussie.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/LED Driving Lights/insanely bright leds.png"
        break;
        
        case "LED Light Bars":
        var text1 ="Torture Tested for Harsh Aussie Conditions",
            text2 ="Combination Spot and Flood Lenses ",
            text3 ="Long Lasting LEDs",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/LED Light Bars/torture tested for harsh aussie conditions.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/LED Light Bars/combination spot and flood lenses.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/LED Light Bars/Long lasting leds.png"
        break;
        
        case "LED Camp Lighting":
        var text1 ="Reliable Lighting Solution",
            text2 ="Quality LEDs",
            text3 ="Made to Last",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/LED Camp Lighting/reliable lighting solutions.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/LED Camp Lighting/quality leds.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/LED Camp Lighting/made to last.png"
        break;
        
        case "Rear Drawers":
        var text1 ="Legendary Strength and Durability",
            text2 ="Smooth Double Roller Bearings",
            text3 ="Quality Components",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Rear Drawers/legendary strength and durability.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Rear Drawers/smooth double roller bearings.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Rear Drawers/quality components.png"
        break;
        
        case "Recovery Accessories":
        var text1 ="Built Tough for Aussie 4WDers",
            icon1 = "https://marketing.4wdsupacentre.com.au/iconsRecovery Accessories/built tough for aussie 4wders.png"
        break;
        
        case "Recovery Kits":
        var text1 ="Quality Parts",
            text2 ="Tough Gear you can Trust",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Recovery Kits/quality parts.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Recovery Kits/tough gear you can trust.png"
        break;
        
        case "Roof Racks":
        var text1 ="Strong and Durable Steel Construction",
            text2 ="Torture Tested on 4WD Action DVD",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Roof Racks/strong and durable steel construction.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Roof Racks/Torture Tested on 4WD Action DVD.png"
        break;
        
        case "Roof Top Tents":
        var text1 ="RipStop Poly Cotton Canvas",
            text2 ="Tough & Durable Construction",
            text3 ="Simple Setup and Pack Down",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Roof Top Tents/ripstop poly cotton canvas.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Roof Top Tents/tough & durable construction.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Roof Top Tents/simple setup and pack down.png"
        break;
        
        case "Solar":
        var text1 ="Weatherproof Regulator",
            text2 ="Efficient Monocrystalline Cells",
            text3 ="Built Tough to Last",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Solar/weatherproof regulator.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Solar/efficient monocrystalline cells.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Solar/built tough to last.png"
        break;
        
        case "Tools":
        var text1 ="Tough Chrome Vanadium Components",
            text2 ="Built By 4WDers for 4WDers",
            text3 ="1000s Sold",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Tools /tough chrome vanadium components.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Tools /built by 4wders for 4wders.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Tools /1000s sold.png"
        break;
        
        case "Winches":
        var text1 = "Battle Tested on 4WD Action DVD",
            text2 = "Tough-as-guts Construction",
            text3 = "Won’t let you Down",
            icon1 = "https://marketing.4wdsupacentre.com.au/icons/Winches/battle tested on 4wd action dvd.png",
            icon2 = "https://marketing.4wdsupacentre.com.au/icons/Winches/tough as guts contruction.png",
            icon3 = "https://marketing.4wdsupacentre.com.au/icons/Winches/wont let you down.png"
        break;
}

var html = '';

if (text1) {
    var item1 = '<div class="opt_item"><div class="optImage"><img src="' + icon1 + '"></div><div class="optp">' + text1 + '</div></div>';
    html += item1
}

if (text2) {
    var item2 = '<div class="opt_item"><div class="optImage"><img src="' + icon2 + '"></div><div class="optp">' + text2 + '</div></div>'
    html += item2
}

if (text3) {
    var item3 = '<div class="opt_item"><div class="optImage"><img src="' + icon3 + '"></div><div class="optp">' + text3 + '</div></div>'
    html += item3
}

    html = '<div class="opt-icon-container">'+html+'</div>'
jQuery('.price-marketing').after(html);